#!/bin/sh

echo DATABASE_URL=$DATABASE_URL >> .env
echo AUTH_PRIVATE_KEY=$AUTH_PRIVATE_KEY >> .env
echo AUTH_PUBLIC_KEY=$AUTH_PUBLIC_KEY >> .env
echo HASURA_ENABLE_CONSOLE=$HASURA_ENABLE_CONSOLE >> .env
echo HASURA_ADMIN_SECRET=$HASURA_ADMIN_SECRET >> .env

echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env

echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env
echo AUTH_IMAGE=$IMAGE:authserver  >> .env

echo export DATABASE_URL=$DATABASE_URL >> .env.source
echo export AUTH_PRIVATE_KEY=\'$AUTH_PRIVATE_KEY\' >> .env.source
echo export AUTH_PUBLIC_KEY=\'$AUTH_PUBLIC_KEY\' >> .env.source
echo export HASURA_ENABLE_CONSOLE=$HASURA_ENABLE_CONSOLE >> .env.source
echo export HASURA_ADMIN_SECRET=$HASURA_ADMIN_SECRET >> .env.source

echo export I_REGISTRY_USER=$CI_REGISTRY_USER   >> .env.source
echo export CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env.source
echo export CI_REGISTRY=$CI_REGISTRY  >> .env.source

echo export IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env.source
echo export AUTH_IMAGE=$IMAGE:authserver  >> .env.source