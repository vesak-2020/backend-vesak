var fs = require("fs");
var fnv = require("fnv-plus");

exports.key = (
  process.env.AUTH_PRIVATE_KEY || fs.readFileSync("private.pem").toString()
).replace(/\\n/g, "\n");

exports.publicKey = (
  process.env.AUTH_PUBLIC_KEY || fs.readFileSync("public.pem").toString()
).replace(/\\n/g, "\n");

exports.kid = process.env.AUTH_KEY_ID || fnv.hash(this.publicKey, 128).hex();
