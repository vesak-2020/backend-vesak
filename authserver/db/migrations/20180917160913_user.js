exports.up = async function(knex, Promise) {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "pgcrypto" schema public');
  return knex.schema.createTable("user", table => {
    table
      .uuid("id")
      .primary()
      .unique()
      .defaultTo(knex.raw("gen_random_uuid()"));
    table
      .string("username")
      .unique()
      .notNullable();
    table
      .string("npm")
      .unique()
      .notNullable();
    table.string("name").notNullable();
    table.string("faculty").notNullable();
    table
      .timestamp("created_at")
      .notNullable()
      .defaultTo(knex.raw("now()"));
    table
      .boolean("active")
      .defaultTo(true)
      .index();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable("user");
};
