const databaseName = "@localhost/vesak";
const pg = require("pg");

const connection_url =
  process.env.DATABASE_URL || `postgres://postgres:${databaseName}`;

module.exports = {
  client: "pg",
  connection: connection_url,
  migrations: {
    directory: __dirname + "/db/migrations"
  }
};
