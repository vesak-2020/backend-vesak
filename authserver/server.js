require("dotenv").config();

const http = require("http");

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const session = require("express-session");

const app = express();
// const userController = require("./controllers/user");

const { User } = require("./db/schema");

const PORT = process.env.PORT || 80;
const SECRET = process.env.SECRET || "secrety";

const whitelist = [
  "http://localhost:8000",
  "http://localhost",
  "https://vesak.id",
  "http://vesak.id"
];
const corsOptions = {
  credentials: true,
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  }
};

const SSO = require("sso-ui");

const sso = new SSO({
  url: "https://api.vesak.id", //required
  session_sso: "sso_user" // defaults to sso_user
});

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(session({ secret: SECRET, resave: true, saveUninitialized: false }));
app.use(sso.middleware);

app.get("/user", sso.login, async function(req, res) {
  const { username, npm, name, faculty } = req.sso_user;
  try {
    let user = await User.query()
      .where("username", username)
      .first()
      .withGraphFetched("roles");

    if (!user) {
      // no user yet
      await User.query()
        .allowGraph("[username, npm, name, faculty]")
        .insert({
          username,
          npm,
          name,
          faculty
        });
      user = await User.query()
        .where("username", username)
        .first()
        .withGraphFetched("roles");
    }
    return res.json(user.getUser());
  } catch (e) {
    console.error(e);
  }
});

app.get("/authenticate", sso.login, function(req, res) {
  if (req.query.next) {
    res.redirect(req.query.next);
  } else {
    res.redirect("https://vesak.id/login");
  }
});

app.get("/logout-sso", sso.logout);

app.get("/logout", sso.clear, function(req, res) {
  res.redirect("https://vesak.id");
});

// app.get("/webhook", userController.getWebhook);
// app.get("/jwks", userController.getJwks);

http.createServer(app).listen(PORT);
